const utils = require('ethereumjs-util')
const config = require('config')
const _ = require('lodash')

module.exports = {
    middleware: () => {
        return (ctx, next) => {

            let signature = ctx.request.headers['x-signature']
            let eth_address = ctx.request.headers['x-eth-address']

            if (!eth_address) ctx.throw(400, 'Provide X-Eth-Address header')
            if (!signature) ctx.throw(400, 'Provide X-Signature header')

            if (!utils.isValidAddress(eth_address)) ctx.throw(400, 'X-Eth-Address is not valid')

            ctx.state.user_address = eth_address.toLowerCase()
            
            let splitSig = signature.split(' ')
            if (splitSig[0] === 'api-key') {

                if (config.get('apiKeys').includes(splitSig[1])) {
                    return next()
                } else {
                    ctx.throw(401, 'Incorrect api-key')
                }

            }

            let sigParams
            try {
                sigParams = utils.fromRpcSig(signature)
            } catch (err) {
                ctx.throw(400, err.message)
            }

            const jsonBody = JSON.stringify({})
            let hash = utils.sha256(jsonBody)

            let correct_eth_address = utils.pubToAddress(utils.ecrecover(hash, sigParams.v, sigParams.r, sigParams.s))
            correct_eth_address = utils.bufferToHex(correct_eth_address)

            if (correct_eth_address.toUpperCase() != eth_address.toUpperCase()) ctx.throw(401, 'X-Eth-Address does not match X-Signature')

            return next()

        }
    }
}