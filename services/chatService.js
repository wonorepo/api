const axios = require('axios')
const conf = require('config')

class ChatService {

    constructor() {
        this._apiChat = axios.create({
            baseURL: `${conf.get('services.chat.url')}/api`,
            headers: {
                'X-Api-Key': ``
            }
        })
    }

    async getChatById(id) {
        let res = await this._apiChat.get(`/chats/${id}`)
        return res.data
    }

    async getChats(params) {
        let res = await this._apiChat.get(`/chats`, { params })
        return res.data
    }

    async getChatMessages(id, params) {
        let res = await this._apiChat.get(`/chats/${id}/messages`, { params })
        return res.data
    }

    async getMessageById(id) {
        let res = await this._apiChat.get(`/messages/${id}`)
        return res.data
    }

    async getCountUnreadMessages(chatId, profileId) {
        let res = await this._apiChat.get(`/chats/${chatId}/messages/count?profileId=${profileId}`)
        return res.data
    }

}

module.exports = new ChatService()