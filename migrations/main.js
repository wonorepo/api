const fs = require('fs')
const path = require('path')
const basename = path.basename(__filename)
const db = require('../db/couchbaseDB')

async function main() {

    const files = fs.readdirSync(__dirname).filter(
        (file) => {
            return (file.indexOf('.') !== 0) && (file !== basename) && (file != "migration_0.js")
        }
    )

    const migration_model = db.buckets.migrations.models.migration

    for (let file of files) {
        
        const docid = migration_model.getDocId(file)
        
        try{
            const doc = await migration_model.getById(docid)
            if (doc) continue
        }catch(err){ }
        
        console.log(`Running migration - ${file}`)

        const migration = require(path.join(__dirname, file))
        await migration.up(db)
        
        await migration_model.create(docid, {}, file)

        console.log(`Migration completed successfully`)

    }

}

db.sync().then(async () => {
    return await main()
}).then(() => {
    process.exit(0)
}).catch((err) => {
    console.log(err.stack)
    process.exit(1)
})

