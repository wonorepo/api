const Koa = require('koa')
const cors = require('@koa/cors')
const bodyParser = require('koa-bodyparser')
const koalogger = require('koa-logger')
const Router = require('koa-router')
const logger = require('./services/logger')

const { ApolloServer } = require('apollo-server-koa');
const schema = require('./schema');

const db = require('./db/couchbaseDB')

const profile_draft = require('./components/profile-draft')
const asset_draft = require('./components/asset-draft')
const profile = require('./components/profile')
const asset = require('./components/asset')
const enums = require('./components/enums')
const chat = require('./components/chat')
const msg = require('./components/msg')
const media = require('./components/media')
const deal = require('./components/deal')
const notification = require('./components/notification')

const router = new Router({ prefix: '/api' })

router.use(profile_draft.api.routes())
router.use(asset_draft.api.routes())
router.use(profile.api.routes())
router.use(asset.api.routes())
router.use(enums.api.routes())
router.use(chat.api.routes())
router.use(msg.api.routes())
router.use(media.api.routes())
router.use(deal.api.routes())
router.use(notification.api.routes())

const app = new Koa()

//apollo-server
const server = new ApolloServer({
	schema,
	context: ()=>{
		return {db}
	}
});

app.on('error', (err) => {
	logger.error(err.stack)
})

app
	.use(cors())
	.use((ctx, next) => {
		ctx.set('Access-Control-Allow-Origin', '*')
		return next()
	})
	.use((ctx, next) => {
		if (ctx.request.method === 'POST' || ctx.request.method === 'PUT') {
			if (!ctx.is('application/json') && !ctx.is('multipart/form-data')) {
				ctx.throw(415)
			}
		}
		return next()
	})
	.use(koalogger())
	.use(bodyParser())
	.use((ctx, next) => {
		ctx.db = db
		ctx.buckets = db.buckets
		ctx.maps = ctx.db.buckets.dictionaries.maps
		return next()
	})
	.use(router.routes())
	.use(router.allowedMethods())

server.applyMiddleware({ app })

module.exports = app