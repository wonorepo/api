const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors = require('./errors')
const asset_errors = require('../asset').errors
const logger = require('../../services/logger')

let api = new Router({ prefix: '/deals' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.entities.models
    return next()
})
api.get('/', async (ctx, next) => {

    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : ['-timestamp_updated']

    const total = await ctx.state.models.deal.count(
        {
            condition: filter
        }
    )

    let deals = await ctx.state.models.deal.find(
        {
            condition: filter,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    if (fields === '*') {

        let promises = []

        for (let deal of deals) {

            const asset_docid = ctx.state.models.asset.getDocId(deal.assetId)
            const profileA_docid = ctx.state.models.profile.getDocId(deal.partyA)
            const profileB_docid = ctx.state.models.profile.getDocId(deal.partyB)

            //assetData 
            promises.push(ctx.state.models.asset.getById(asset_docid))

            //members
            promises.push(ctx.state.models.profile.getById(profileA_docid))
            promises.push(ctx.state.models.profile.getById(profileB_docid))

        }

        let res = await Promise.all(promises)

        let index = 0
        for (let i = 0; i < deals.length; i++) {

            //assetData
            deals[i].assetData = res[index++]

            //members
            deals[i].members = [res[index++], res[index++]]

        }

    }

    ctx.status = 200
    ctx.body = {
        count: deals.length,
        total: total,
        deals: deals
    }

})
api.get('/:address', async (ctx, next) => {

    const { address } = ctx.params;

    try {

        const docid = ctx.state.models.deal.getDocId(address)
        let deal = await ctx.state.models.deal.getById(docid)

        ctx.status = 200
        ctx.body = deal

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundDeal)
        }
        throw err
    }

})
api.use(verifier.middleware())
api.post('/', async (ctx, next) => {

    const body = ctx.request.body

    try {

        const res = ctx.state.models.deal.validate(body)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        const docid_profile = ctx.state.models.profile.getDocId(ctx.state.user_address)
        body.eth_address = body.eth_address.toLowerCase()

        body.profile_id = docid_profile
        body.owner = ctx.state.user_address
        body.timestamp_created = Date.now()

        const id = body.eth_address
        const docid = ctx.state.models.deal.getDocId(id)
        const deal = await ctx.state.models.deal.create(docid, body, id)

        ctx.status = 201
        ctx.body = deal

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyAlreadyExists) {
            ctx.throw(409, errors.dealAlreadyExist)
        }
        throw err
    }

})
api.put('/:address', async (ctx, next) => {

    const { address } = ctx.params;
    const updates = ctx.request.body

    try {

        const res = ctx.state.models.deal.validate(updates)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        updates.timestamp_updated = Date.now()

        const docid = ctx.state.models.deal.getDocId(address)
        let deal = await ctx.state.models.deal.update(docid, updates)

        ctx.status = 200
        ctx.body = deal

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundDeal)
        }
        throw err
    }

})
api.delete('/:address', async (ctx, next) => {

    const { address } = ctx.params;

    try {

        const docid = ctx.state.models.deal.getDocId(address)
        await ctx.state.models.deal.remove(docid)

        ctx.status = 204
        ctx.body = docid

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundDeal)
        }
        throw err
    }

})

module.exports = api