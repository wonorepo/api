
module.exports = (deal, _, ctx) => {

    const asset = ctx.db.buckets.entities.models.asset
    const docid = asset.getDocId(deal.assetId)

    return asset.getById(docid)
    
}