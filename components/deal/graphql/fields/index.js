const assetData = require('./assetData')
const members = require('./members')

module.exports = {
    Deal: {
        assetData,
        members
    }
}