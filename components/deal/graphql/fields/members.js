
module.exports = (deal, _, ctx) => {

    const profile = ctx.db.buckets.entities.models.profile
    const profileA_docid = profile.getDocId(deal.partyA)
    const profileB_docid = profile.getDocId(deal.partyB)

    return Promise.all([
        profile.getById(profileA_docid),
        profile.getById(profileB_docid)
    ])
    
}