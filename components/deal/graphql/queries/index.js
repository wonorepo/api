const deal = require('./deal')
const deals = require('./deals')

module.exports = {
    Query: {
        deal,
        deals
    }
}