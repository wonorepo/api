
module.exports = (_, args, ctx)=>{
    const deal = ctx.db.buckets.entities.models.deal
    const id = args.id
    const docid =  deal.getDocId(id)
    return deal.getById(docid)
}