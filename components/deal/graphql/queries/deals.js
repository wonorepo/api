
module.exports = async (parent, args, ctx, info) => {
    
    const deal = ctx.db.buckets.entities.models.deal
    const filter = args.filter || ''
    const sort = args.sort ? args.sort.split(',') : []

    const total = await deal.count({condition: filter})
    const deals = await deal.find({condition: filter, sort: sort, limit: args.limit, offset: args.offset})
    
    return {
        count: deals.length,
        total: total,
        deals: deals
    }

}