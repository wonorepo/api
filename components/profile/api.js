const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors = require('./errors')
const asset_errors = require('../asset-draft').errors
const draft_api = require('../profile-draft').api
const logger = require('../../services/logger')
const chatService = require('../../services/chatService')

let api = new Router({ prefix: '/profiles' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.entities.models
    ctx.state.draft_models = ctx.buckets.drafts.models
    ctx.state.asset_errors = asset_errors
    ctx.state.errors = errors
    return next()
})

api.get('/:address/assets', draft_api.route('read-assets').stack[0])
api.get('/:address/assets/:id', draft_api.route('read-asset').stack[0])
api.get('/:address', draft_api.route('read').stack[0])
api.get('/', draft_api.route('read-profiles').stack[0])
api.get('/:address/asset-drafts', async (ctx, next) => {

    const { address } = ctx.params
    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let profile_docid = ctx.state.models.profile.getDocId(address.toLowerCase())

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    let condition =
        `profile_id = "${profile_docid}"` +
        `${filter ? ` AND (${filter})` : ''}`

    const total = await ctx.state.draft_models.asset.count(
        {
            condition: condition
        }
    )

    let assets = await ctx.state.draft_models.asset.find(
        {
            condition: condition,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: assets.length,
        total: total,
        assets: assets
    }

})
api.get('/:address/chats', async (ctx, next) => {

    const { address } = ctx.params
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort || '-timestamp_created'
    
    delete ctx.query.fields

    ctx.query.filter =
        `"'${address.toLowerCase()}' IN members` +
        `${filter ? ` AND (${filter})` : ''}"`
    ctx.query.sort = sort

    let resChats = await chatService.getChats(ctx.query)

    let promises = []
    for (let chat of resChats.chats) {

        const asset_docid = ctx.state.models.asset.getDocId(chat.asset)

        //assetData
        promises.push(ctx.state.models.asset.getById(asset_docid))

        //lastMsg
        promises.push(chatService.getChatMessages(chat.id, {
            sort: '-timestamp_created',
            limit: 1,
            offset: 0
        }))

        //unreadedMsg
        promises.push(chatService.getCountUnreadMessages(chat.id, address))

    }

    let res = await Promise.all(promises)
    let index = 0
    for (let i = 0; i < resChats.chats.length; i++) {

        //assetData
        resChats.chats[i].assetData = res[index++]

        //lastMsg
        const lastMsg = res[index++].messages
        if (lastMsg.length != 0) {
            resChats.chats[i].lastMsg = lastMsg[0]
        } else {
            resChats.chats[i].lastMsg = null
        }

        //unreadedMsg
        resChats.chats[i].unreadedMsg = res[index++].count

    }

    resChats.chats.sort((a, b) => {
        return b.lastMsg.timestamp_created - a.lastMsg.timestamp_created
    })

    ctx.status = 200
    ctx.body = resChats

})
api.use(verifier.middleware())
api.post('/', async (ctx, next) => {

    const body = ctx.request.body

    try {

        const res = ctx.state.models.profile.validate(body)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        const docid = ctx.state.models.profile.getDocId(ctx.state.user_address)

        body.owner = ctx.state.user_address

        const docid_profile_draft = ctx.state.draft_models.profile.getDocId(body.owner)
        await ctx.state.draft_models.profile.update(docid_profile_draft, {
            pending: false
        })

        const profile = await ctx.state.models.profile.create(docid, body)

        ctx.status = 201
        ctx.body = profile

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyAlreadyExists) {
            ctx.throw(409, ctx.state.errors.profileAlreadyExist)
        }
        throw err
    }

})
api.put('/:address', async (ctx, next) => {

    const { address } = ctx.params;
    const updates = ctx.request.body

    if (ctx.state.user_address.toUpperCase() != address.toUpperCase()) {
        ctx.throw(400, ctx.state.errors.notMatchEthAddress)
    }

    try {

        const res = ctx.state.models.profile.validate(updates)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        try {
            const docid_profile_draft = ctx.state.draft_models.profile.getDocId(updates.owner)
            await ctx.state.draft_models.profile.update(docid_profile_draft, {
                pending: false
            })
        } catch (err) {
            logger.error(err.stack)
        }

        const docid = ctx.state.models.profile.getDocId(ctx.state.user_address)
        let profile = await ctx.state.models.profile.update(docid, updates)

        ctx.status = 200
        ctx.body = profile

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundProfile)
        }
        throw err
    }

})
api.delete('/:address', draft_api.route('delete').stack[0])

module.exports = api