const api = require('./api')
const schema = require('./schema')
const errors = require('./errors')
const graphql = require('./graphql')

module.exports = { api,  schema, errors, graphql }