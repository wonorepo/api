

module.exports = (_, args, ctx)=>{
    const profile = ctx.db.buckets.entities.models.profile
    const id = args.id
    const docid =  profile.getDocId(id)
    return profile.getById(docid)
}