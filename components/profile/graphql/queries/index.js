const profile = require('./profile')

module.exports = {
    Query: {
        profile
    }
}