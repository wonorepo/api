const fs = require('fs')
const schema = fs.readFileSync(`${__dirname}/schema.graphql`)

const types = schema.toString()
const queries = require('./queries')
//const fields = require('./fields')

module.exports = { types, queries }