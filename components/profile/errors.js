
module.exports = {
    notMatchEthAddress: 'X-Eth-Address does not match address in url params.',
    notFoundProfile: 'Could not find profile.',
    profileAlreadyExist: 'Profile already exists.'
}