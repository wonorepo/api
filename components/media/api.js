const Router = require('koa-router')
const multer = require('koa-multer')
const send = require('koa-send')
const conf = require('config')
const verifier = require('../../services/verifier')
const uuidv4 = require('uuid/v4')

function format(file) {
    delete file.destination
    delete file.path
    return file
}

const storagePath = conf.get("mediaStorage.path")

const storage = multer.diskStorage({
    destination: function (ctx, file, cb) {
        cb(null, storagePath)
    },
    filename: function (ctx, file, cb) {
        cb(null, uuidv4())
    }
})

const upload = multer({ 
    storage: storage, 
    limits: {
        fields: 1,
        fileSize: 5000000,
        files: 1
    } 
}).any()

let api = new Router({ prefix: '/media' })

api.get('/:filename', async (ctx, next) => {

    const { filename } = ctx.params
    const contentType = ctx.query.contentType

    if (contentType) ctx.set('Content-Type', contentType)

    await send(ctx, filename, { root: storagePath })

})
//api.use(verifier.middleware())
api.post('/', upload, async (ctx, next) => {

    let files = []
    if (ctx.req.files && ctx.req.files.length != 0) {
        files = ctx.req.files.map(format)
    }

    ctx.status = 200
    ctx.body = {
        count: files.length,
        files: files
    }

})

module.exports = api