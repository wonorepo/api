const Router = require('koa-router')
const chatService = require('../../services/chatService')
const errors = require('./errors')

let api = new Router({ prefix: '/chats' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.entities.models
    return next()
})

api.get('/:id/messages', async (ctx, next) => {

    const { id } = ctx.params

    try {

        let res = await chatService.getChatMessages(id, ctx.query)

        ctx.status = 200
        ctx.body = res

    } catch (err) {
        if (err.response && err.response.status === 404) ctx.throw(404, errors.notFoundChat)
        throw err
    }

})
api.get('/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        let chat = await chatService.getChatById(id)

        const asset_docid = ctx.state.models.asset.getDocId(chat.asset)
        const asset = await ctx.state.models.asset.getById(asset_docid)
        const profile = await ctx.state.models.profile.getById(asset.profile_id)

        chat.assetOwner = profile

        ctx.status = 200
        ctx.body = chat

    } catch (err) {
        if (err.response && err.response.status === 404) ctx.throw(404, errors.notFoundChat)
        throw err
    }

})
api.get('/', async (ctx, next) => {

    let res = await chatService.getChats(ctx.query)

    ctx.status = 200
    ctx.body = res

})

module.exports = api