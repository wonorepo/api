
module.exports = {
    notMatchEthAddress: 'X-Eth-Address does not match address in url params.',
    notFoundChat: 'Could not find chat.',
    chatAlreadyExist: 'Chat already exist'
}