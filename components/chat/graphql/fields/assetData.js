
module.exports = (chat, _, ctx) => {

    const asset = ctx.db.buckets.entities.models.asset
    const docid = asset.getDocId(chat.asset)

    return asset.getById(docid)
    
}