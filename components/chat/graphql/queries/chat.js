
module.exports = (_, args, ctx)=>{
    const chat = ctx.db.buckets.entities.models.chat
    const id = args.id
    const docid =  chat.getDocId(id)
    return chat.getById(docid)
}