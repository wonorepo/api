const chat = require('./chat')
const chats = require('./chats')

module.exports = {
    Query: {
        chat,
        chats
    }
}