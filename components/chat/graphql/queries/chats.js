
module.exports = async (parent, args, ctx, info) => {
    
    const chat = ctx.db.buckets.entities.models.chat
    const filter = args.filter || ''
    const sort = args.sort ? args.sort.split(',') : []
    
    const total = await chat.count({condition: filter})
    const chats = await chat.find({condition: filter, sort: sort, limit: args.limit, offset: args.offset})
    
    return {
        count: chats.length,
        total: total,
        chats: chats
    }

}