const Router = require('koa-router')

let api = new Router({ prefix: '/enums' })

api.get('/', async (ctx, next) => {

    let body = {}
    for (let map_name in ctx.maps) {
        body[map_name] = await ctx.maps[map_name].getMap()
    }

    ctx.status = 200
    ctx.body = body

})

module.exports = api
