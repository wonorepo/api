const Router = require('koa-router')
const chatService = require('../../services/chatService')
const errors = require('./errors')

let api = new Router({ prefix: '/messages' })

api.get('/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        let msg = await chatService.getMessageById(id)

        ctx.status = 200
        ctx.body = msg

    } catch (err) {
        if (err.response && err.response.status === 404) ctx.throw(404, errors.notFoundMsg)
        throw err
    }

})

module.exports = api