
module.exports = {
    notFoundMsg: 'Could not find message.',
    notProvidedChatId: 'Not provided chatId parameter'
}