const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors = require('./errors')
const errors_profile = require('../profile').errors
const uuidv4 = require('uuid/v4')

let api = new Router({ prefix: '/notifications' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.entities.models
    return next()
})

api.get('read', '/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        const docid = ctx.state.models.notification.getDocId(id)
        let notification = await ctx.state.models.notification.getById(docid)

        ctx.status = 200
        ctx.body = notification

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundNotification)
        }
        throw err
    }

})
api.use(verifier.middleware())
api.post('create', '/', async (ctx, next) => {

    const body = ctx.request.body

    const res = ctx.state.models.notification.validate(body)
    if (!res.valid) {
        ctx.status = 400
        ctx.body = {
            message: 'Validation error',
            errors: res.errors
        }
        return
    }

    try {
        const docid_profile = ctx.state.models.profile.getDocId(ctx.state.user_address)
        await ctx.state.models.profile.getById(docid_profile)
    } catch (err) {
        if (err.code && err.code == ctx.db.couchbase.errors.keyNotFound) {
            ctx.throw(404, errors_profile.notFoundProfile)
        }
        throw err
    }

    body.owner = ctx.state.user_address
    body.timestamp_created = Date.now()

    let id = uuidv4()
    const docid = ctx.state.models.notification.getDocId(id)

    const notification = await ctx.state.models.notification.create(docid, body, id)

    ctx.status = 201
    ctx.body = notification

})
api.put('update', '/:id', async (ctx, next) => {

    const { id } = ctx.params;

    const updates = ctx.request.body

    try {

        const res = ctx.state.models.notification.validate(updates)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        updates.timestamp_updated = Date.now()

        const docid = ctx.state.models.notification.getDocId(id)
        let notification = await ctx.state.models.notification.update(docid, updates)

        ctx.status = 200
        ctx.body = notification

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundNotification)
        }
        throw err
    }

})
api.delete('delete', '/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        const docid = ctx.state.models.notification.getDocId(id)
        await ctx.state.models.notification.remove(docid)

        ctx.status = 204

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, errors.notFoundNotification)
        }
        throw err
    }

})

module.exports = api