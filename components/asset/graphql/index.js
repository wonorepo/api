const fs = require('fs')
const schema = fs.readFileSync(`${__dirname}/schema.graphql`)

const types = schema.toString()

const queries = require('./queries')


module.exports = { types, queries }