const asset = require('./asset')
const assets = require('./assets')

module.exports = {
    Query: {
        asset,
        assets
    }
}