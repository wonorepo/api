
module.exports = async (parent, args, ctx, info) => {
    
    const asset = ctx.db.buckets.entities.models.asset
    const filter = args.filter || ''
    const sort = args.sort ? args.sort.split(',') : []
    
    const total = await asset.count({condition: filter})
    const assets = await asset.find({condition: filter, sort: sort, limit: args.limit, offset: args.offset})
    
    return {
        count: assets.length,
        total: total,
        assets: assets
    }

}