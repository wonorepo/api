
module.exports = (_, args, ctx)=>{
    const asset = ctx.db.buckets.entities.models.asset
    const id = args.id
    const docid =  asset.getDocId(id)
    return asset.getById(docid)
}