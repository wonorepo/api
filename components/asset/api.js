const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors_profile = require('../profile').errors
const errors = require('./errors')
const draft_api = require('../asset-draft').api
const logger = require('../../services/logger')

let api = new Router({ prefix: '/assets' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.entities.models
    ctx.state.draft_models = ctx.buckets.drafts.models
    ctx.state.errors = errors
    ctx.state.errors_profile = errors_profile
    return next()
})

api.get('/:id', draft_api.route('read').stack[0])
api.get('/', async (ctx, next) => {

    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort
    const search_term = ctx.query.search

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []



    if (search_term && search_term != '*') {

        let res = await ctx.state.models.asset.find(
            {
                condition: filter,
                attributes: ['META().id AS docid']
            }
        )

        let assets = (await ctx.state.models.asset.find(
            {
                condition: filter,
                attributes: attrArr,
                sort: sortArr,
                limit: limit,
                offset: offset
            }
        )).filter((asset) => asset.desc && asset.desc.toUpperCase().indexOf(search_term.toUpperCase()) != -1
            || asset.location && asset.location.toUpperCase().indexOf(search_term.toUpperCase()) != -1
            || asset.title && asset.title.toUpperCase().indexOf(search_term.toUpperCase()) != -1)

        // let docids = res.map(item => item.docid || item.id)
        //
        // let sq = ctx.state.models.asset.SearchQuery
        //
        // let qp = sq.conjuncts(
        //     sq.docIds(docids)
        // )
        //
        // qp.and(sq.disjuncts(
        //     sq.matchPhrase(search_term).field("desc"),
        //     sq.match(search_term).field("location").fuzziness(1),
        //     sq.match(search_term).field("title").fuzziness(1)
        // ))
        //
        // let query = sq.new(`${ctx.state.models.asset.bucketName}-asset-index`, qp)
        //
        // sortArr.push("-_score")
        //
        // query
        //     .limit(limit)
        //     .skip(offset)
        //     .sort(sortArr)
        //     .fields(attrArr)
        //
        // let searchRes = await ctx.state.models.asset.search(query)
        //
        // let assets = await ctx.state.models.asset.getMulti(searchRes.map(item => item.id))

        let promises = []
        for (let asset of assets) {
            //assetOwner
            if (!asset.pending) {
                promises.push(ctx.state.models.profile.getById(asset.profile_id))
            }
        }

        let profiles = await Promise.all(promises)

        let index = 0
        for (let i = 0; i < assets.length; i++) {
            //assetOwner
            assets[i].assetOwner = profiles[index++]
        }

        ctx.status = 200
        ctx.body = {
            count: assets.length,
            assets: assets
        }

        return

    }

    const total = await ctx.state.models.asset.count(
        {
            condition: filter
        }
    )

    let assets = await ctx.state.models.asset.find(
        {
            condition: filter,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    let promises = []
    for (let asset of assets) {
        //assetOwner
        if (!asset.pending) {
            promises.push(ctx.state.models.profile.getById(asset.profile_id))
        }
    }

    let profiles = await Promise.all(promises)

    let index = 0
    for (let i = 0; i < assets.length; i++) {
        //assetOwner
        assets[i].assetOwner = profiles[index++]
    }

    ctx.status = 200
    ctx.body = {
        count: assets.length,
        total: total,
        assets: assets
    }

})
api.get('/:id/deals', async (ctx, next) => {

    const { id } = ctx.params
    const limit = ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort || '-timestamp_created'

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    let condition =
        `assetId = ${id}` +
        `${filter ? ` AND (${filter})` : ''}`

    const total = await ctx.state.models.deal.count(
        {
            condition: condition
        }
    )

    let deals = await ctx.state.models.deal.find(
        {
            condition: condition,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: deals.length,
        total: total,
        deals: deals
    }

})
api.use(verifier.middleware())
api.post('/', async (ctx, next) => {

    const body = ctx.request.body

    const res = ctx.state.models.asset.validate(body)
    if (!res.valid) {
        ctx.status = 400
        ctx.body = {
            message: 'Validation error',
            errors: res.errors
        }
        return
    }

    try {
        var docid_profile = ctx.state.models.profile.getDocId(ctx.state.user_address)
        await ctx.state.models.profile.getById(docid_profile)
    } catch (err) {
        if (err.code && err.code == ctx.db.couchbase.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors_profile.notFoundProfile)
        }
        throw err
    }

    body.profile_id = docid_profile
    body.owner = ctx.state.user_address

    try {
        const docid_asset_draft = ctx.state.draft_models.asset.getDocId(body.id)
        await ctx.state.draft_models.asset.remove(docid_asset_draft)
    } catch (err) {
        logger.error(err.stack)
    }

    const id = await ctx.state.models.asset.getId()
    const docid = ctx.state.models.asset.getDocId(id)
    const asset = await ctx.state.models.asset.create(docid, body, id)

    ctx.status = 201
    ctx.body = asset

})
api.put('/:address', async (ctx, next) => {

    let { address } = ctx.params;

    address = address.toLowerCase()

    if (address.slice(0, 2) != "0x") return next()

    const updates = ctx.request.body

    console.log(updates)

    const res = ctx.state.models.asset.validate(updates)
    if (!res.valid) {
        ctx.status = 400
        ctx.body = {
            message: 'Validation error',
            errors: res.errors
        }
        return
    }

    let condition = `eth_address="${address}"`

    let oldAsset = await ctx.state.models.asset.findOne({ condition: condition })

    if (!oldAsset) ctx.throw(404, ctx.state.errors.notFoundAsset)

    let docid = ctx.state.models.asset.getDocId(oldAsset.id)
    let newAsset = await ctx.state.models.asset.update(docid, updates)

    ctx.status = 200
    ctx.body = newAsset

})
api.put('/:id', draft_api.route('update').stack[0])
api.delete('/:id', draft_api.route('delete').stack[0])

module.exports = api
