const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors_profile = require('../profile-draft').errors
const errors = require('./errors')

let api = new Router({ prefix: '/asset-drafts' })

api.use((ctx, next) => {
    ctx.state.models = ctx.buckets.drafts.models
    ctx.state.errors = errors
    ctx.state.errors_profile = errors_profile
    return next()
})

api.get('read', '/:id', async (ctx, next) => {

    const { id } = ctx.params

    try {

        const docid = ctx.state.models.asset.getDocId(id)
        let asset = await ctx.state.models.asset.getById(docid)

        ctx.status = 200
        ctx.body = asset

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundAsset)
        }
        throw err
    }

})
api.get('read-assets', '/', async (ctx, next) => {

    const limit =  ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort
    const search_term = ctx.query.search

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    if (search_term && search_term != '*') {

        let res = await ctx.state.models.asset.find(
            {
                condition: filter,
                attributes: ['META().id AS docid']
            }
        )

        let docids = res.map(item => item.docid)
        
        let sq = ctx.state.models.asset.SearchQuery

        let qp = sq.conjuncts(
            sq.docIds(docids)
        )

        qp.and(sq.disjuncts(
            sq.matchPhrase(search_term).field("desc"),
            sq.match(search_term).field("location").fuzziness(1),
            sq.match(search_term).field("title").fuzziness(1)
        ))

        let query = sq.new(`${ctx.state.models.asset.bucketName}-asset-index`, qp)
        
        sortArr.push("-_score")
        
        query
            .limit(limit)
            .skip(offset)
            .sort(sortArr)
            .fields(attrArr)

        let searchRes = await ctx.state.models.asset.search(query)

        let assets = await ctx.state.models.asset.getMulti(searchRes.map(item => item.id))
        
        ctx.status = 200
        ctx.body = {
            count: assets.length,
            assets: assets
        }

        return

    }

    const total = await ctx.state.models.asset.count(
        {
            condition: filter
        }
    )

    let assets = await ctx.state.models.asset.find(
        {
            condition: filter,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: assets.length,
        total: total,
        assets: assets
    }

})

api.use(verifier.middleware())

api.post('create', '/', async (ctx, next) => {

    const body = ctx.request.body

    const res = ctx.state.models.asset.validate(body)
    if (!res.valid) {
        ctx.status = 400
        ctx.body = {
            message: 'Validation error',
            errors: res.errors
        }
        return
    }

    const docid_profile = ctx.state.models.profile.getDocId(ctx.state.user_address)

    body.profile_id = docid_profile
    body.owner = ctx.state.user_address

    const id = await ctx.state.models.asset.getId()
    const docid = ctx.state.models.asset.getDocId(id)
    const asset = await ctx.state.models.asset.create(docid, body, id)

    ctx.status = 201
    ctx.body = asset

})
api.put('update', '/:id', async (ctx, next) => {

    const { id } = ctx.params;
    const updates = ctx.request.body

    try {

        const res = ctx.state.models.asset.validate(updates)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        const docid = ctx.state.models.asset.getDocId(id)
        let asset = await ctx.state.models.asset.update(docid, updates)

        ctx.status = 200
        ctx.body = asset

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundAsset)
        }
        throw err
    }

})
api.delete('delete', '/:id', async (ctx, next) => {

    const { id } = ctx.params;

    try {

        const docid = ctx.state.models.asset.getDocId(id)
        await ctx.state.models.asset.remove(docid)

        ctx.status = 204
        ctx.body = docid

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundAsset)
        }
        throw err
    }

})

module.exports = api