const Router = require('koa-router')
const verifier = require('../../services/verifier')
const errors = require('./errors')
const asset_errors = require('../asset-draft').errors

let api = new Router({ prefix: '/profile-drafts' })

api.use((ctx, next) => {
    ctx.state.errors = errors
    ctx.state.asset_errors = asset_errors
    ctx.state.models = ctx.buckets.drafts.models
    return next()
})

api.get('read-assets', '/:address/assets', async (ctx, next) => {

    const { address } = ctx.params
    const limit =  ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let profile_docid = ctx.state.models.profile.getDocId(address.toLowerCase())

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    let condition =
        `profile_id = "${profile_docid}"` +
        `${filter ? ` AND (${filter})` : ''}`


    const total = await ctx.state.models.asset.count(
        {
            condition: condition
        }
    )

    let assets = await ctx.state.models.asset.find(
        {
            condition: condition,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: assets.length,
        total: total,
        assets: assets
    }

})
api.get('read-asset', '/:address/assets/:id', async (ctx, next) => {

    const { address } = ctx.params
    const { id } = ctx.params

    let profile_docid = ctx.state.models.profile.getDocId(address.toLowerCase())

    let condition =
        `profile_id = "${profile_docid}"` +
        ` AND id = ${id}`

    let res = await ctx.state.models.asset.findOne(
        {
            condition: condition
        }
    )

    if (!res) ctx.throw(404, ctx.state.asset_errors.notFoundAsset)

    ctx.status = 200
    ctx.body = res[ctx.state.models.profile.bucketName]

})
api.get('read', '/:address', async (ctx, next) => {

    const { address } = ctx.params;

    try {

        const docid = ctx.state.models.profile.getDocId(address.toLowerCase())
        let profile = await ctx.state.models.profile.getById(docid)

        ctx.status = 200
        ctx.body = profile

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundProfile)
        }
        throw err
    }

})
api.get('read-profiles', '/', async (ctx, next) => {

    const limit =  ctx.query.limit ? Number.parseInt(ctx.query.limit) : 10
    const offset = ctx.query.offset ? Number.parseInt(ctx.query.offset) : 0
    const fields = ctx.query.fields || '*'
    const filter = ctx.query.filter ? ctx.query.filter.slice(1, -1) : ''
    const sort = ctx.query.sort

    let attrArr = fields.split(',')
    let sortArr = sort ? sort.split(',') : []

    const total = await ctx.state.models.profile.count(
        {
            condition: filter
        }
    )

    let profiles = await ctx.state.models.profile.find(
        {
            condition: filter,
            attributes: attrArr,
            sort: sortArr,
            limit: limit,
            offset: offset
        }
    )

    ctx.status = 200
    ctx.body = {
        count: profiles.length,
        total: total,
        profiles: profiles
    }

})

api.use(verifier.middleware())
api.post('create', '/', async (ctx, next) => {

    const body = ctx.request.body

    try {

        const res = ctx.state.models.profile.validate(body)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        
        const docid = ctx.state.models.profile.getDocId(ctx.state.user_address)

        body.owner = ctx.state.user_address

        const profile = await ctx.state.models.profile.create(docid, body)

        ctx.status = 201
        ctx.body = profile

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyAlreadyExists) {
            ctx.throw(409, ctx.state.errors.profileAlreadyExist)
        }
        throw err
    }

})
api.put('update', '/:address', async (ctx, next) => {

    const { address } = ctx.params;
    const updates = ctx.request.body

    if (ctx.state.user_address.toUpperCase() != address.toUpperCase()) {
        ctx.throw(400, ctx.state.errors.notMatchEthAddress)
    }

    try {

        const res = ctx.state.models.profile.validate(updates)
        if (!res.valid) {
            ctx.status = 400
            ctx.body = {
                message: 'Validation error',
                errors: res.errors
            }
            return
        }

        const docid = ctx.state.models.profile.getDocId(ctx.state.user_address)
        let profile = await ctx.state.models.profile.update(docid, updates)

        ctx.status = 200
        ctx.body = profile

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundProfile)
        }
        throw err
    }

})
api.delete('delete', '/:address', async (ctx, next) => {

    const { address } = ctx.params;

    if (ctx.state.user_address.toUpperCase() != address.toUpperCase()) {
        ctx.throw(400, ctx.state.errors.notMatchEthAddress)
    }

    try {

        const docid = ctx.state.models.profile.getDocId(ctx.state.user_address)
        await ctx.state.models.profile.remove(docid)

        ctx.status = 204

    } catch (err) {
        if (err.code && err.code == ctx.db.errors.keyNotFound) {
            ctx.throw(404, ctx.state.errors.notFoundProfile)
        }
        throw err
    }

})

module.exports = api