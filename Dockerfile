ARG NODE_VERSION=10.11
FROM node:${NODE_VERSION}

RUN npm install pm2 -g

COPY package.json yarn.lock /api/
RUN cd /api && yarn install --production

COPY . /api
WORKDIR /api

ARG ENV=dev
ENV NODE_ENV=${ENV}

EXPOSE 3001

CMD ["pm2-runtime", "process.yml"]