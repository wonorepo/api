const { merge } = require('lodash')
const { makeExecutableSchema } = require('graphql-tools')

const asset = require('./components/asset').graphql
const deal = require('./components/deal').graphql
const profile = require('./components/profile').graphql
const chat = require('./components/chat').graphql

const root = `
	type Query {
		dummy: String
	}

	type Mutation {
		dummy: String
	}

	type Subscription {
		dummy: String
	}

	schema {
		query: Query
		mutation: Mutation
		subscription: Subscription
	}
`

const resolvers = merge(
	{},
	asset.queries,
	deal.queries,
	deal.fields,
	profile.queries,
	chat.queries,
	chat.fields
)

const schema = makeExecutableSchema({
	typeDefs: [
		root,
		asset.types,
		deal.types,
		profile.types,
		chat.types
	],
	resolvers,
})

module.exports = schema