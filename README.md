## Asset ##

GET /assets

    Query Params:
        filter
        search
        sort
        fields
        limit
        offset
    
    Example: 
        ?filter="id=1 and title='Ford'" - filter="<whereClause>" more details here: https://docs.couchbase.com/server/6.0/n1ql/n1ql-language-reference/where.html
        ?sort=id - ASC sort
        ?sort=-id - DESC sort
        ?fields=id,title - only id and title fields
        ?limit=10,offset=1

GET /assets/:id

GET /assets/:id/deals

    Query Params:
        filter
        sort
        fields
        limit
        offset

POST /assets

    Body:
    {
        "category": int,
        "title": string,
        "desc": string,
        "price": int,
        "per": string,
        "location": string,
        "extras": object,
        "imageHash": string,
        "dates": object,
        "owner": string,
        "profile_id": string,
        "ipfs_hash": string,
        "is_deleted": boolean
    }

PUT /assets/:id

    Body:
    {
        "category": int,
        "title": string,
        "desc": string,
        "price": int,
        "per": string,
        "location": string,
        "extras": object,
        "imageHash": string,
        "dates": object,
        "owner": string,
        "profile_id": string,
        "ipfs_hash": string,
        "is_deleted": boolean
    }

DELETE /assets/:id

## Asset Draft ##

CRUD operations

endpoint: /asset-drafts

## Profile ##

GET /profiles/:address

POST /profiles

    Body:
    {
        "address": string,
        "birthday": object,
        "firstname": string,
        "ipfs_hash": string,
        "is_deleted": boolean,
        "lastname": object,
        "owner": string,
        "photo": string,
        "photoHash": string,
    }

PUT /profiles
    
    Body:
    {
        "address": string,
        "birthday": object,
        "firstname": string,
        "ipfs_hash": string,
        "is_deleted": boolean,
        "lastname": object,
        "owner": string,
        "photo": string,
        "photoHash": string,
    }

DELETE /profiles/:address

GET /profiles/:address/assets

    Query Params:
        filter
        sort
        fields
        limit
        offset

GET /profiles/:address/asset-drafts

    Query Params:
        filter
        sort
        fields
        limit
        offset
    
GET /profiles

    Query Params:
        filter
        sort
        fields
        limit
        offset

GET /profiles/:address/chats

## Profile Draft ##

CRUD operations
endpoint: /profile-drafts

## Deal ##

CRUD operations
endpoint: /deals

## Enum ##
Get list of the dictionaries:

GET /enums

## Chat ##

GET /chats

    Query Params:
        filter
        sort
        fields
        limit
        offset

GET /chats/:id

GET /chats/:id/messages

    Query Params:
        filter
        sort
        fields
        limit
        offset

## Message ##

GET /messages/:id

## Full text search index ##

FTS configs are located in project repo: db/fts_indexes

To update the fts index see the documentation (https://docs.couchbase.com/server/6.0/fts/fts-creating-indexes.html#index-creation-with-the-rest-api)