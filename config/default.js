module.exports = {

    port: 3001,
    couchbase: {
        protocol: 'couchbase',
        host: 'localhost',
        fts_port: 8094,
        username: 'Administrator',
        password: '123456'
    },
    apiKeys: [
        '50b39511-7817-49c5-90e6-53ddbdd95c83'
    ],
    mediaStorage: {
        path: '/Users/nikitazaharov/tmp/uploads'
    },
    services: {
        chat: {
            url: 'http://localhost:3000'
        }
    }

}
