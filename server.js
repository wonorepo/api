const db = require('./db/couchbaseDB')
const config = require('config')
const logger = require('./services/logger')

logger.info(`Couchbase DB sync...`)
db.sync().then(async () => {
    logger.info(`Couchbase DB synchronized`)
    const app = require('./app')
    app.listen(config.get('port'))
    logger.info(`Server is running on port ${config.get('port')}`)
}).catch((err)=>{
    logger.error(err.stack)
    process.exit(1)
})