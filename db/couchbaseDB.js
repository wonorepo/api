const Promise = require("bluebird");
const config = require('config')
const couchbase = require('couchbase')
const logger = require('../services/logger')
const buckets = require('./setup').buckets
const { Model, Dictionary } = require('../components/base')
const axios = require('axios')
Promise.promisifyAll(couchbase);

class CouchbaseDB {

    constructor(conf) {
        this._couchbase = couchbase
        this._cluster = new couchbase.Cluster(conf.host)
        this._cluster.authenticate(conf.username, conf.password)
        this._ftsApi = axios.create({
            baseURL: `${conf.protocol}://${conf.username}:${conf.password}@${conf.host}:${conf.fts_port}/api`,
            headers: {
                'cache-control': 'no-cache',
                'application-type': 'application/json'
            }
        })
        this._manager = this._cluster.manager()
        this._errors = this._couchbase.errors
        this._buckets = {}
    }

    async sync() {

        let listNameBuckets = (await this.listBuckets()).map(bucket => bucket.name)

        for (let bucket of buckets) {

            let bucketName = bucket.name
            let models = bucket.models || []
            let maps = bucket.maps || []
            let indexes = bucket.indexes || []
            let fts_indexes = bucket.fts_indexes || []
            let bucket_opts = bucket.opts || {}

            if (!listNameBuckets.includes(bucketName)) {
                logger.debug(`[bucket-${bucketName}] Creating bucket...`)
                await this._createBucket(bucketName, bucket_opts)
                await this._delay(3000)
                logger.debug(`[bucket-${bucketName}] Bucket is created`)
            }

            logger.debug(`[bucket-${bucketName}] Opening bucket...`)
            this._openBucket(bucketName)
            logger.debug(`[bucket-${bucketName}] Bucket is opened`)

            for (let model of models) {

                let counter = model.counter
                let modelName = model.name

                if (counter) {
                    
                    counter.name = `counter::${modelName}`

                    logger.debug(`[counter] Creating ${counter.name} counter...`)
                    await this._createCounter(counter.name, bucketName, counter.opt)
                    logger.debug(`[counter] Counter is created`)

                }

                logger.debug(`[model] Creating ${modelName} model...`)
                this.buckets[bucketName].models[modelName] = new Model({
                    model: model,
                    bucket: this.buckets[bucketName].obj,
                    cb: this._couchbase
                })
                logger.debug(`[model] Model is created`)

            }

            for (let map of maps) {

                let map_id = map.name
                let obj = map.obj

                logger.debug(`[map] Creating ${map_id} map...`)
                await this._createMap(map_id, obj, bucketName)
                this.buckets[bucketName].maps[map_id] = new Dictionary(map_id, this.buckets[bucketName].obj)
                logger.debug(`[map] Map is created`)

            }

            for (let index of indexes) {

                let attribute = index.attribute

                let indexName = `${bucketName}-${attribute}-index`

                logger.debug(`[index] Creating ${indexName} index...`)
                await this._createIndex(indexName, attribute, bucketName)
                logger.debug(`[index] Index is created`)

            }

            for (let fts_index of fts_indexes) {

                let fts_index_name = fts_index.config.name
                let fts_config = fts_index.config

                logger.debug(`[fts-index] Creating ${fts_index_name} index...`)
                await this._createIndexFTS(fts_config)
                logger.debug(`[fts-index] Index is created`)

            }

        }

    }

    get errors() {
        return this._errors
    }

    get buckets() {
        return this._buckets
    }

    get cluster() {
        return this._cluster
    }

    get couchbase() {
        return this._couchbase
    }

    async _createBucket(name, opts = {}) {
        return new Promise((resolve, reject) => {
            this._manager.createBucket(name, opts, function (err) {
                if (err) reject(err)
                resolve(name)
            })
        })
    }

    _openBucket(name) {
        let bucket = {
            obj: {},
            models: {},
            maps: {},
            fts_indexes: {}
        }
        bucket.obj = this._cluster.openBucket(name)
        this._buckets[name] = bucket
    }

    async listBuckets() {
        return new Promise((resolve, reject) => {
            this._manager.listBuckets((err, res) => {
                if (err) reject(err)
                resolve(res)
            })
        })
    }

    async _delay(timeout) {
        return new Promise((resolve, reject) => {
            setTimeout(() => { resolve() }, timeout)
        })
    }

    async _createCounter(docid, bucketName, opt = { initial: 0 }) {

        try {
            await this._buckets[bucketName].obj.getAsync(docid)
            return docid
        } catch (err) {
            if (err.code && err.code == this._couchbase.errors.keyNotFound) {
                await this._buckets[bucketName].obj.counterAsync(docid, 1, opt)
                return
            }
            throw err
        }

    }

    async _createIndex(indexName, attribute, bucketName) {
        let bucket = this.buckets[bucketName].obj
        let manager = bucket.manager()
        return new Promise((resolve, reject) => {
            manager.createIndex(indexName, [attribute], { ignoreIfExists: true }, (err, res) => {
                if (err) reject(err)
                resolve(res)
            })
        })
    }

    async _createMap(map_id, map, bucketName) {
        let bucket = this.buckets[bucketName].obj
        await bucket.upsertAsync(map_id, map)
    }

    async _getIndexFTS() {
        let res = await this._ftsApi.get('/index')
        console.log(res)
    }

    async _createIndexFTS(indexConfig) {

        try {
            let res = await this._ftsApi.get(`/index/${indexConfig.name}`)
            indexConfig.uuid = res.data.indexDef.uuid
        } catch (err) { }

        await this._ftsApi.put(`/index/${indexConfig.name}`, indexConfig)

    }

}

let db = new CouchbaseDB(config.get('couchbase'))
module.exports = db