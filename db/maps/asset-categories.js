
module.exports = Object.freeze(
    {
        name: 'asset_categories',
        obj: {
            APPARTAMENT: {
                name: 'Apartment',
                id: 1
            },
            TRANSPORT: {
                name: 'Vehicle',
                id: 2
            },
            JOB: {
                name: 'Job',
                id: 3
            },
            FREELANCER : {
                name: 'Freelancer',
                id: 4
            },
        }
    }
)