
module.exports = Object.freeze(
    {
        name: 'asset_features',
        obj: {
            FEATURE_1: {
                name: 'Feature_1',
                id: 1
            },
            FEATURE_2: {
                name: 'Feature_2',
                id: 2
            },
            FEATURE_3: {
                name: 'Feature_3',
                id: 3
            }
        }
    }
)