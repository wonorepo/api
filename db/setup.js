
module.exports = Object.freeze({
    buckets: [
        {
            name: 'drafts',
            opts: {
                ramQuotaMB: 100
            },
            models: [
                {
                    name: 'profile',
                    schema: require('../components/profile-draft/schema'),
                    counter: {
                        opt: { initial: 0 }
                    }
                },
                {
                    name: 'asset',
                    schema: require('../components/asset-draft/schema'),
                    counter: {
                        opt: { initial: 0 }
                    }
                }
            ],
            indexes: [
                {
                    attribute: 'type'
                },
                {
                    attribute: 'profile_id'
                }
            ],
            fts_indexes: [
                {
                    config: require('./fts_indexes/drafts-asset-index')
                }
            ]
        },
        {
            name: 'entities',
            opts: {
                ramQuotaMB: 100
            },
            models: [
                {
                    name: 'profile',
                    schema: require('../components/profile/schema'),
                    counter: {
                        opt: { initial: 0 }
                    }
                },
                {
                    name: 'asset',
                    schema: require('../components/asset/schema'),
                    counter: {
                        opt: { initial: 0 }
                    }
                },
                {
                    name: 'msg',
                    schema: require('../components/msg/schema')
                },
                {
                    name: 'chat',
                    schema: require('../components/chat/schema')
                },
                {
                    name: 'deal',
                    schema: require('../components/deal/schema')
                },
                {
                    name: 'notification',
                    schema: require('../components/notification/schema')
                }
            ],
            indexes: [
                {
                    attribute: 'type'
                },
                {
                    attribute: 'profile_id'
                },
                {
                    attribute: 'chat_id'
                }
            ],
            fts_indexes: [
                {
                    config: require('./fts_indexes/entities-asset-index')
                }
            ]
        },
        {
            name: 'dictionaries',
            opts: {
                ramQuotaMB: 100
            },
            maps: require('./maps')
        },
        {
            name: 'migrations',
            opts: {
                ramQuotaMB: 100
            },
            models: [
                {
                    name: 'migration'
                }
            ]
        }
    ]
})